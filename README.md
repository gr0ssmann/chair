# chair 🪑

Shows a countdown. Useful for session chairs at conferences.

Let's suppose you have a session running from 15:34 ("3:34pm" in American parlance) to 16:40 ("4:40pm") and you have three speakers. This means every speaker has a slot of 22 minutes. In `main.py` can define this as follows:

```python
slots = ["15:34", "15:56", "16:18", "16:40"]
```

Let's suppose it is now 15:30. This program will then show a countdown to 15:34. However, once the session starts, it will go to the next slot. In other words, from 15:34, the countdown would run until 15:56, and then from 15:56 to 16:18 and so on.

`main.py` also allows you to scale the font size. This software is "hackable": You might want to change some of the internals (but you don't have to). See `countdown/__init__.py`.

Download using `git clone https://gitlab.com/gr0ssmann/chair.git`. Run using `python main.py`, or `python3 main.py`, or `./main.py`, depending on your setup.

On Linux, run `xset s off -dpms` to prevent your screen from sleeping.

Enjoy!

## License

Honestly, most of this was built using ChatGPT. As far as I am concerned, all code is CC0:

Unless otherwise noted, everything in this repository is licensed under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/), or (at your option) any later version.
