#!/usr/bin/env python3

import countdown

slots = ["15:34", "15:56", "16:18", "16:40"]

scale = 0.3  # scale font size

countdown.show(slots, scale)
