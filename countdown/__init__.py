import math

import tkinter as tk
from datetime import datetime


class Countdown(tk.Frame):
    def __init__(self, master, slots, scale):
        self.slots = slots
        self.scale = scale

        super().__init__(master)
        self.create_widgets()
        self.show_time()

    def create_widgets(self):
        screen_width = self.master.winfo_screenwidth()
        screen_height = self.master.winfo_screenheight()

        self.master.geometry(f"{screen_width}x{screen_height}")

        font_size = round(self.scale * min(screen_width, screen_height))
        self.time_label = tk.Label(self, font=("Helvetica", font_size, "bold"))
        self.time_label.pack(fill="both", expand=True)

    def show_time(self):
        now = datetime.now()

        # acquire next target

        while True:
            try:
                cur_slot = self.slots[0]

                if isinstance(cur_slot, str) and len(cur_slot) in (5, 8):
                    if len(cur_slot) == 5:
                        target = datetime.strptime(cur_slot, "%H:%M")
                    else:
                        target = datetime.strptime(cur_slot, "%H:%M:%S")

                    target = target.replace(year=now.year, month=now.month, day=now.day)
                elif isinstance(cur_slot, datetime):
                    target = cur_slot
                else:
                    raise ValueError(
                        "Slots have the wrong type. Must be str or datetime objects."
                    )
            except IndexError:
                raise ValueError("Slots are empty or all in the past.")

            if target > now:
                break
            else:
                print(f"removing {target}")
                self.slots.pop(0)

        delta = math.floor((target - now).total_seconds())

        if delta < 0:
            self.time_label.configure(text="The countdown is over!")
        else:
            min_ = delta // 60
            sec_ = delta - min_ * 60

            countdown_string = f"{min_}:{sec_:02}"

            self.time_label.configure(fg="white")

            if delta <= 60:
                self.time_label.configure(bg="red")
            elif delta <= 300:
                self.time_label.configure(bg="orange", fg="black")
            elif delta <= 600:
                self.time_label.configure(bg="blue")
            else:
                self.time_label.configure(bg="black")

            self.time_label.configure(text=countdown_string)

            self.after(1000, self.show_time)


def show(slots, scale):
    root = tk.Tk()
    root.attributes("-fullscreen", True)

    frame = Countdown(master=root, slots=slots, scale=scale)
    frame.pack(fill="both", expand=True)

    root.mainloop()
